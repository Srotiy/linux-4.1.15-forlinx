#!/bin/bash 
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- distclean
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- imx_v7_defconfig
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- menuconfig
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- zImage -j6
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- dtbs
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- modules -j6
